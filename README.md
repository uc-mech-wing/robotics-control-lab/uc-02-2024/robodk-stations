RoboDK Stations
===============

Introduction
-----------

__RoboDK Stations__ contains the [RoboDK][robodk] _station files_ that students
are to use as the basis of their assignment. These station files (also known as
_RDK files_) are specific to a particular robot / coffee cart combination.

Warnings
--------

It is of __UTMOST IMPORTANCE__ that students use the station file corresponding
to the coffee cart apparatus to which they have been assigned. Failure to do so
will result in differences in behaviour when running offline (simulation only)
and online (virtual and physical robots synchronised). This is likely to result
in damage to both the robot and tooling due to variations in the location of
fixtures between carts.

In order to reduce the chances that students will select the incorrect station
file, the name of each cart is prominently displayed in multiple locations on
the cart itself. Station files have then been named after the coffee cart
apparatus to which they pertain.

| Cart Name | Station File     | Robot Details                |
| --------- | -----------------| ---------------------------- |
| Robot 1   | Robot_1_2024.rdk | UR5 serial number 2019350471 |
| Robot 2   | Robot_2_2024.rdk | UR5 serial number 2020350569 |
| Robot 3   | Robot_3_2024.rdk | UR5 serial number 2023350546 |

In addition to an accurate model of the coffee cart after which it is named,
each station file also contains a number of so-called _robot programs_.

Tool Programs
-------------

The first group of robot programs comprise those that students __MUST__ use to
perform certain real-world tasks, specifically:

 - Changing robot tools
 - Actuating robot tools
 - Actuating cart fixtures

This group of robot programs are located in the _Tool_Programs_ directory of
the RoboDK station tree.

If called when online - that is, when a socket connection has been established
between the laboratory computer running RoboDK and the physical robot - these
programs will result in synchronous changes to the state of the coffee cart
apparatus in both simulation and the real world. This includes movement of the
robot, actuation of cart fixtures, and so on. If called when offline, only the
state of the simulation will change.

Visual Programs
---------------

The second group of programs only change the state of the simulation, and have
no real-world effect whether they are called when online or offline. A classic
example of this type of program is the one that may be used to reset the state
of the simulation when things go awry.

This group of programs are located in the _Visual_Programs_ directory of the
RoboDK station tree.

Python Programs
---------------

In addition to robot programs, each station file also includes several example
Python programs. These are intended to provide students with a framework to use
as the basis for their own code, and also to serve as a gentle introduction to
the RoboDK [Python API][python_api].

This group of programs are located in the _Python_Programs_ directory of the
RoboDK station tree.

These Python programs may be called when online or offline, but some will have
a different effect when run on the laboratory computers directly connected to
the coffee carts. This includes the program that requests a Modbus Ethernet
server daemon to provide the current scale weight (grams), or to tare (zero)
the scale. It is only when called from a computer on the same network as the
scales that the daemon is able to respond to such requests. 

<h2>robodk_basics</h2>

Before attempting to run the _robodk_basics_ module, it is important to clarify
if the intent is to run offline or online.

If the intent is to run online, the first step it to ensure that each of the
_robot programs_ called by the module have their _Run on robot_ option
__selected__. This may be done by right-clicking on the applicable program in
the station tree and ensuring there is a check mark next to the _Run on robot_
option. A faster method however is to first select all of the robot programs by
shift-clicking, then when the _Run on robot_ option is selected, the change
will be applied to all robot programs at once.

Next, establish a socket connection between the laboratory computer running
RoboDK and the physical robot by selecting _Connect -> Connect robot_ from the
RoboDK menu bar. A small docked window titled _Connection to UR5_ will then
appear below the station tree. From within this window, press the _Connect_
button and wait for the _Connection status_ bar display to change from
_disconnected_ to _connected_. Once the _Connection status_ bar has turned
green, press the _Get Position_ button to synchronise the position of the
simulated robot with the physical robot. The module is now ready to be run -
simply right-click on it in the station tree and select _Run on Robot_ to run
online.

If the intent is to run offline, the first step is to ensure that each of the
_robot programs_ called by the module have their _Run on robot_ option
__deselected__. This may be done by right-clicking on the applicable program in
the station tree and ensuring there is no check mark next to the _Run on robot_
option. A faster method however is to first select all of the robot programs by
shift-clicking, then when the _Run on robot_ option is selected, the change
will be applied to all robot programs at once.

Next, close the socket connection between the laboratory computer running
RoboDK and the physical robot by pressing the _Disconnect_ button in the
_Connection to UR5_ window. The module is now ready to be run - simply
right-click on it in the station tree and select _Run_ to run offline.

<h2>robodk_scales</h2>

Before attempting to run the _robodk_scales_ module, there are two operations
that must first be performed. The first is to ensure that the requirements of
the __modbus-client__ repository upon which _robodk_scales_ depends have been
satisfied. Per the repository _README_ file, these dependencies must be
installed using the correct RoboDK Python executable - that is:

    C:\RoboDK\Python-Embedded\python.exe

The simplest way to ensure that the dependencies are installed correctly is to
clone the repository, then use the included _requirements.txt_ file as follows,
replacing _&lt;path&gt;_ with the path to the requirements file:

    C:\> C:\RoboDK\Python-Embedded\python -m pip install -r C:\<path>\requirements.txt

This will install the required packages to:

    C:\RoboDK\Python-Embedded\Lib\site-packages\

If the intent is to run _robodk_scales_ from within RoboDK, it is necessary to
first open the _scale_client_ module for editing. This is due to a quirk with
RoboDK in which it stores copies of Python files in a temporary location, and
thus makes running Python code externally the preferred option. See below for
further details.

Tips & Tricks
-------------

Python modules may be run from within RoboDK by right-clicking as described
above, or externally. In the case of _robodk_basics_ and _robodk_scales_,
running from within RoboDK may appear to be the simplest option as they are
both included within the RDK station tree. Student code may be run in a similar
manner by either dragging and dropping the file into the station tree, or by
selecting _File -> Open..._ from the RoboDK menu bar.

However, from a code security and maintenance point of view, it is far better
to run Python code externally. This may be done by opening a terminal (Linux)
or command prompt (Windows) and running the relevant module using the Python
executable shipped with RoboDK. For example, in the case of Windows:

    C:\RoboDK\Python-Embedded\python C:\<path>\student_code.py

Documentation
-------------

Code has been documented using [Doxygen][doxygen].

License
-------

__RoboDK Stations__ is released under the [GNU General Public License][gpl].

Authors
-------

Code by Rodney Elliott, <rodney.elliott@canterbury.ac.nz>

[robodk]: https://robodk.com
[python_api]: https://robodk.com/doc/en/PythonAPI/index.html
[doxygen]: https://www.doxygen.nl
[gpl]: https://www.gnu.org/licenses/gpl-3.0.html
